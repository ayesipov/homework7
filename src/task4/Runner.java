package task4;


import java.time.LocalDateTime;

import java.util.List;

public class Runner {
    public static void main(String[] args) {
        Task firsTask = new Task(11, "Private Project", "Private Project description",
                LocalDateTime.of(2019,4,22,12,12), Priority.MAJOR
        );

        Task secondTask = new Task(33778, "Public Project", "Public Project description",
                LocalDateTime.of(2019,5,12,22,11), Priority.NORMAL
        );

        Task thirdTask = new Task(23423423, "Some Project", "Some Project description",
                LocalDateTime.of(2018,1,1,1,11), Priority.MINOR
        );

        Tasks allTasksByDeadline = new Tasks().add(firsTask).add(secondTask).add(thirdTask);
        Tasks allTasksByName = new Tasks().add(firsTask).add(secondTask).add(thirdTask);
        Tasks allTasksByPriority = new Tasks().add(firsTask).add(secondTask).add(thirdTask);


        allTasksByDeadline.sort(TaskComparators.byDeadline());
        allTasksByName.sort(TaskComparators.byName());
        allTasksByPriority.sort(TaskComparators.byPriority());

        System.out.println(allTasksByDeadline);
//        Tasks{tasks=[Task{id=23423423, name='Some Project', description='Some Project description', deadline=2018-01-01T01:11, priority=MINOR}
//, Task{id=11, name='Private Project', description='Private Project description', deadline=2019-04-22T12:12, priority=MAJOR}
//, Task{id=33778, name='Public Project', description='Public Project description', deadline=2019-05-12T22:11, priority=NORMAL}
//]}
        System.out.println(allTasksByName);
//        Tasks{tasks=[Task{id=11, name='Private Project', description='Private Project description', deadline=2019-04-22T12:12, priority=MAJOR}
//, Task{id=33778, name='Public Project', description='Public Project description', deadline=2019-05-12T22:11, priority=NORMAL}
//, Task{id=23423423, name='Some Project', description='Some Project description', deadline=2018-01-01T01:11, priority=MINOR}
//]}

        System.out.println(allTasksByPriority);
//        Tasks{tasks=[Task{id=23423423, name='Some Project', description='Some Project description', deadline=2018-01-01T01:11, priority=MINOR}
//, Task{id=33778, name='Public Project', description='Public Project description', deadline=2019-05-12T22:11, priority=NORMAL}
//, Task{id=11, name='Private Project', description='Private Project description', deadline=2019-04-22T12:12, priority=MAJOR}
//]}


        List<Task> filteredByExpiderDeadline = allTasksByDeadline.filter(TaskPredicates.byHasExpiredDeadline(LocalDateTime.of(2019,3,12,10,1)));
        System.out.println(filteredByExpiderDeadline);
        //[Task{id=23423423, name='Some Project', description='Some Project description', deadline=2018-01-01T01:11, priority=MINOR}

        List<Task> filterByNameContainse = allTasksByName.filter(TaskPredicates.byNameContains("bli"));
        System.out.println(filterByNameContainse);
        //[Task{id=33778, name='Public Project', description='Public Project description', deadline=2019-05-12T22:11, priority=NORMAL}

        List<Task> filterByPriority = allTasksByPriority.filter(TaskPredicates.byHasPriority(Priority.MAJOR));
        System.out.println(filterByPriority);
        //[Task{id=11, name='Private Project', description='Private Project description', deadline=2019-04-22T12:12, priority=MAJOR}

    }





}
