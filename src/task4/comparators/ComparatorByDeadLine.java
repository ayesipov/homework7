package task4.comparators;

import task4.Task;

import java.util.Comparator;

public class ComparatorByDeadLine implements Comparator<Task> {

    @Override
    public int compare(Task o1, Task o2) {
        return o1.getDeadline().compareTo(o2.getDeadline());
    }
}
