package task4;

import task4.comparators.ComparatorByDeadLine;
import task4.comparators.ComparatorByName;
import task4.comparators.ComparatorByPriority;

import java.util.Comparator;

public class TaskComparators {

    public static Comparator<Task> byName(){
        return new ComparatorByName();
    }

    public static Comparator<Task> byDeadline(){
        return new ComparatorByDeadLine();
    }

    public static Comparator<Task> byPriority(){
        return new ComparatorByPriority();
    }

}
