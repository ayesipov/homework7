package task4;

import java.time.LocalDateTime;
import java.util.Objects;

public class Task {

    private final long id;
    private final String name;
    private final String description;
    private final LocalDateTime deadline;
    private final Priority priority;

    public Task(long id, String name, String description, LocalDateTime deadline, Priority priority) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.deadline = deadline;
        this.priority = priority;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public LocalDateTime getDeadline() {
        return deadline;
    }

    public Priority getPriority() {
        return priority;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return id == task.id &&
                Objects.equals(name, task.name) &&
                Objects.equals(description, task.description) &&
                Objects.equals(deadline, task.deadline) &&
                priority == task.priority;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, deadline, priority);
    }

    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", deadline=" + deadline +
                ", priority=" + priority +
                '}'+"\n";


    }
}
