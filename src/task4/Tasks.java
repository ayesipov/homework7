package task4;

import task3.Filter;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;

public class Tasks {

    private final List<Task> tasks = new ArrayList<>();

    public List<Task> getTasks() {
        return tasks;
    }

    public Tasks add (Task task){
        tasks.add(task);
        return this;
    }

    public List<Task> sort(Comparator comparator) {
        tasks.sort(comparator);
        return tasks;
    }

    public List<Task> filter(Predicate predicate) {
        return Filter.filter(tasks, predicate);
    }

    @Override
    public String toString() {
        return "Tasks{" +
                "tasks=" + tasks +
                '}';
    }


}
