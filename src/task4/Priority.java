package task4;

public enum Priority {
    MINOR,
    NORMAL,
    MAJOR
}
