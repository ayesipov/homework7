package task4;

import task4.predicates.PredicateByHasExpiredDeadline;
import task4.predicates.PredicateByHasPriority;
import task4.predicates.PredicateNameContains;

import java.time.LocalDateTime;
import java.util.function.Predicate;

public class TaskPredicates {

    public static Predicate<Task> byNameContains(String nameString){
        return new PredicateNameContains(nameString);
    }

    public static Predicate<Task> byHasExpiredDeadline(LocalDateTime deadlineTime){
        return new PredicateByHasExpiredDeadline(deadlineTime);
    }

    public static Predicate<Task> byHasPriority(Priority priority){
        return new PredicateByHasPriority(priority);
    }

}
