package task4.predicates;

import task4.Task;

import java.time.LocalDateTime;
import java.util.function.Predicate;

public class PredicateByHasExpiredDeadline implements Predicate<Task> {

    private final LocalDateTime deadlineTime;

    public PredicateByHasExpiredDeadline(LocalDateTime deadlineTime) {
        this.deadlineTime = deadlineTime;
    }


    @Override
    public boolean test(Task task) {
        return task.getDeadline().isBefore(deadlineTime);
    }
}
