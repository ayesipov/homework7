package task4.predicates;

import task4.Task;

import java.util.function.Predicate;

public class PredicateNameContains implements Predicate<Task> {

    private final String nameString;

    public PredicateNameContains(String nameString) {
        this.nameString = nameString;
    }

    @Override
    public boolean test(Task task) {
        return task.getName().contains(nameString);
    }
}
