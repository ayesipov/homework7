package task4.predicates;

import task4.Priority;
import task4.Task;

import java.util.function.Predicate;

public class PredicateByHasPriority implements Predicate<Task> {

    private final Priority priority;

    public PredicateByHasPriority(Priority priority) {
        this.priority = priority;
    }

    @Override
    public boolean test(Task task) {
        return task.getPriority().equals(priority);
    }
}
