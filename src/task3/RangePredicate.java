package task3;

import java.util.function.Predicate;

public class RangePredicate implements Predicate<Integer> {
    private final int max;
    private final int min;

    public RangePredicate(int max, int min) {
        this.max = max;
        this.min = min;
    }


    @Override
    public boolean test(Integer integer) {
        return integer >= min && integer <= max;
    }
}
