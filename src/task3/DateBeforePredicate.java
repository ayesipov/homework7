package task3;

import java.time.LocalDate;
import java.util.function.Predicate;

public class DateBeforePredicate implements Predicate<LocalDate> {

    private final LocalDate date;

    public DateBeforePredicate(LocalDate date) {
        this.date = date;
    }


    @Override
    public boolean test(LocalDate localDate) {
        return localDate.compareTo(date) < 0;
    }
}
