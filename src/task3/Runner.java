package task3;

import java.lang.reflect.Array;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class Runner {

    public static void main(String[] args) {

        List<Integer> ages = Arrays.asList(30,18,76,20,16,93);
        List<Integer> adultPeople = Filter.filter(ages, new RangePredicate(100,18));
        System.out.println("Adult people in the List: " + adultPeople); // Adult people in the List: [30, 18, 76, 20, 93]

        List<LocalDate> dates = Arrays.asList(
          LocalDate.of(1922,11,22),
          LocalDate.of(1995,10,2),
          LocalDate.of(2000,10,2),
          LocalDate.of(2001,10,2)
        );

        List<LocalDate> before21st = Filter.filter(dates,new DateBeforePredicate(LocalDate.of(2000,1,1)));
        System.out.println(before21st); // [1922-11-22, 1995-10-02]

    }
}
