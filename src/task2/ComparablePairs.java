package task2;

public class ComparablePairs <T extends Comparable> {

    private T o1;
    private T o2;


    public ComparablePairs(T o1, T o2) {
        this.o1 = o1;
        this.o2 = o2;
    }

    public T biggest(){
        if (o1.compareTo(o2) >= 0){
            return o1;
        }else {
            return o2;
        }
    }

    public T smallest(){
        if (o1.compareTo(o2) < 0){
            return o1;
        } else {
            return o2;
        }
    }
}
