package task2;

import java.time.LocalDate;

public class Runner {
    public static void main(String[] args) {
        ComparablePairs<String> names = new ComparablePairs<>("Nick","Valera");
        System.out.println("Biggest Name: " + names.biggest()); // Valera
        System.out.println("Smallest Name: " + names.smallest()); // Nick

        ComparablePairs<Integer> value = new ComparablePairs<>(33,17);
        System.out.println("Biggest int: " + value.biggest()); // 33
        System.out.println("Smallest int: " + value.smallest()); // 17

        ComparablePairs<LocalDate> localDate = new ComparablePairs<>(
                LocalDate.of(1111,1,11),
                LocalDate.of(2222,2,22)
        );
        System.out.println("Biggest LocalDate: " + localDate.biggest()); // 2222-02-22
        System.out.println("Smallest LocalDate: " + localDate.smallest()); // 1111-01-11

    }

}
