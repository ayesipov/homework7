package task1;

import java.util.Comparator;
import java.util.List;

public class Max {
    public static final <T> T max(List<T> list, Comparator<T> comparator) {
        return list.stream().max(comparator).orElseThrow();
    }
}
