package task1;

import java.time.LocalDate;
import java.util.List;

public class Runner {
    public static void main(String[] args) {
        List<String> stringList = List.of("FirstString", "SecondStringLong", "ThirdStr");
        System.out.println("Longest String: " + Max.max(stringList, new LenngthComparator())); // SecondStringLong

        List<LocalDate> localDateList = List.of(
                LocalDate.of(1995,7,29),
                LocalDate.of(1990,8,11),
                LocalDate.of(1989,12,25),
                LocalDate.of(1989,12,23)
        );

        System.out.println("Max value of days from year: " + Max.max(localDateList, new DayInYearComparator())); // 1989-12-25
    }
}
